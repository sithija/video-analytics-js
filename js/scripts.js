/**
 * Created by PhpStorm.
 * User: Sithija
 * Date: 8/27/2018
 * Time: 1:00 PM
 */

$(function () {
    (function (win, doc, $) {
        let report_count = 0;
        let filterLables = {
            videoid: 'Video',
            domain: 'Domain',
            devicetype: 'Device'
        };
        let charts = [];
        let config = (function () {

            let valid_request = true;
            let apiEndPoint = 'https://analytics-api.ivideosmart.com/v1/reporting/{0}/video-analytics';
            let filtered_by = [];
            let params = {
                property_type: {value: null, required: true},
                property_id: {value: null, required: true},
                start_date: {value: null, required: false},
                end_date: {value: null, required: false},
                filters: {value: null, required: false},
                timezone: {value: null, required: false}
            };
            let reports_info = {
                summery: {
                    name: 'Summery',
                    active: true,
                    renderFunc: 'renderSummery',
                    metrics: 'pageviews,videoviews,videoviews_midpoint,videoviews_complete,uniquevisitors',
                    dimensions: '',
                    pagelimit: 1,
                    // htmlId: ['video-view-count']
                    htmlId: [
                        'page-view-count',
                        'video-view-count',
                        'video-views-50',
                        'video-views-100',
                        'unique-visitors'
                    ]
                },
                pageViewsByDateGraph: {
                    name: 'Page views by date',
                    active: true,
                    renderFunc: 'renderAreaChart',
                    metrics: 'pageviews',
                    dimensions: 'date',
                    pagelimit: 200,
                    htmlId: 'page-views-chart'
                },
                videoViewsByDateGraph: {
                    name: 'Video views by date',
                    active: true,
                    renderFunc: 'renderAreaChart',
                    metrics: 'videoviews',
                    dimensions: 'date',
                    pagelimit: 200,
                    htmlId: 'video-views-chart'
                },
                doughnutChart: {
                    name: 'Doughnut Chart',
                    active: false
                },
                byCountry: {
                    name: 'Views by country',
                    active: true,
                    renderFunc: 'renderTable',
                    metrics: 'pageviews,videoviews,videoviews_midpoint,videoviews_complete',
                    dimensions: 'country',
                    pagelimit: 10,
                    htmlId: 'by-country',
                    headers: ['Country', 'Page Views', 'Page Views %', 'Video Views', 'Video Views %', 'Video Views (Midpoint)', 'Video Views (Complete)'],
                    percentageIndexes: [1, 2] // Index of metrics to show percentage. Start from 1.
                },
                byBrowser: {
                    name: 'Views by browser',
                    active: true,
                    renderFunc: 'renderTable',
                    metrics: 'pageviews,videoviews,videoviews_midpoint,videoviews_complete',
                    dimensions: 'browser',
                    pagelimit: 10,
                    htmlId: 'by-browser',
                    headers: ['Browser', 'Page Views', 'Page Views %', 'Video Views', 'Video Views %', 'Video Views (Midpoint)', 'Video Views (Complete)'],
                    percentageIndexes: [1, 2] // Index of metrics to show percentage. Start from 1.
                },
                byDomain: {
                    name: 'Views by domain',
                    active: true,
                    renderFunc: 'renderTable',
                    metrics: 'pageviews,videoviews,videoviews_midpoint,videoviews_complete',
                    dimensions: 'domain',
                    pagelimit: 10,
                    htmlId: 'by-domain',
                    headers: ['Domain', 'Page Views', 'Page Views %', 'Video Views', 'Video Views %', 'Video Views (Midpoint)', 'Video Views (Complete)'],
                    percentageIndexes: [1, 2], // Index of metrics to show percentage. Start from 1.
                    linkedCol: {0: 0}, // Index start from 0.
                    hiddenOnFilters: ['domain']
                },
                byVideo: {
                    name: 'Views by video',
                    active: true,
                    renderFunc: 'renderTable',
                    metrics: 'pageviews,videoviews,videoviews_midpoint,videoviews_complete',
                    dimensions: 'videoid,videotitle',
                    pagelimit: 10,
                    htmlId: 'by-video',
                    headers: ['Video', 'Page Views', 'Page Views %', 'Video Views', 'Video Views %', 'Video Views (Midpoint)', 'Video Views (Complete)'],
                    percentageIndexes: [2, 3], // Index of metrics to show percentage. Skip count of dimensions.
                    linkedCol: {1: 0}, // Index start from 0. Key is the index to link. Values is the column to get filter.
                    skipCol: [0],
                    hiddenOnFilters: ['videoid']
                },
                byDevice: {
                    name: 'Views by device',
                    active: true,
                    renderFunc: 'renderTable',
                    metrics: 'pageviews,videoviews,videoviews_midpoint,videoviews_complete',
                    dimensions: 'devicetype',
                    pagelimit: 10,
                    htmlId: 'by-device',
                    headers: ['Device', 'Page Views', 'Page Views %', 'Video Views', 'Video Views %', 'Video Views (Midpoint)', 'Video Views (Complete)'],
                    percentageIndexes: [1, 2], // Index of metrics to show percentage. Skip count of dimensions.
                    linkedCol: {0: 0}, // Index start from 0. Key is the index to link. Values is the column to get filter.
                    hiddenOnFilters: ['devicetype']
                },
                byOwner: {
                    name: 'Views by video owner',
                    active: false
                },
                byCategory: {
                    name: 'Views by category',
                    active: false
                },
                byPage: {
                    name: 'Views by page',
                    active: false
                }
            };

            let getDefaultValue = function (field) {
                let date = new Date();
                switch (field) {
                    case 'start_date':
                        let days = 7;//date.getDate() == 31 ? 31 : 30;
                        date.setDate(date.getDate() - days);
                        return date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
                    case 'end_date':
                        return date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
                    case 'filters':
                        return {};
                    case 'timezone':
                        return 'utc';
                    default:
                        return null;
                }
            };

            (function () {
                let urlParams = new URLSearchParams(location.search);
                let paramList = '';

                for (let param in params) {
                    if (valid_request && urlParams.has(param)) {
                        params[param].value = urlParams.get(param);
                    } else if (params[param].required) {
                        valid_request = false;
                    } else {
                        params[param].value = getDefaultValue(param);
                    }
                    paramList += param + ', ';
                }

                if (!valid_request) {
                    paramList = paramList.substring(0, paramList.length - 2);
                    alert('Parameter(s) missing. Accepted parameters: ' + paramList);
                    return;
                }

                $('#start_date').val(params.start_date.value);
                $('#end_date').val(params.end_date.value);
                $('#timezone').val(params.timezone.value);
            })();

            let InnerFunc = function () {
                this.getPropertyType = function () {
                    return params.property_type.value;
                };
                this.getPropertyId = function () {
                    return params.property_id.value;
                };
                this.getStartDate = function () {
                    return params.start_date.value;
                };
                this.setStartDate = function (val) {
                    params.start_date.value = val;
                };
                this.getEndDate = function () {
                    return params.end_date.value;
                };
                this.setEndDate = function (val) {
                    params.end_date.value = val;
                };
                this.getFilteredBy = function () {
                    return filtered_by;
                };
                this.setFilteredBy = function (filters) {
                    filtered_by = filters;
                };
                this.getReportsInfo = function () {
                    return reports_info;
                };
                this.getApiEndPoint = function () {
                    return apiEndPoint.replace('{0}', this.getPropertyType() + '/' + this.getPropertyId());
                };
                this.getStatus = function () {
                    return valid_request;
                };
                this.getFilters = function () {
                    return params.filters.value;
                };
                this.setFilters = function (filterType, filterValue, displayValue) {
                    params.filters.value[filterType] = {
                        filterValue: filterValue,
                        displayValue: displayValue
                    };
                    populateFilters();
                };
                this.deleteFilter = function (filterType) {
                    delete params.filters.value[filterType];
                    populateFilters();
                };
                this.getTimezone = function () {
                    return params.timezone.value;
                }
                this.setTimezone = function (value) {
                    params.timezone.value = value;
                }
            };

            return new InnerFunc();
        })();

        let Report = function (info) {
            let that = this;

            this.render = function () {
                let data = {
                    date1: config.getStartDate(),
                    date2: config.getEndDate(),
                    dimensions: info.dimensions,
                    metrics: info.metrics,
                    pagelimit: info.pagelimit,
                    timezone: config.getTimezone()
                };

                let filterCount = 0;
                let strFilters = '';
                let objFilters = config.getFilters();
                for (let key in objFilters) {
                    strFilters += key + '$' + objFilters[key].filterValue + ',';
                    filterCount++;
                }
                if (filterCount > 0) {
                    strFilters = strFilters.substr(0, strFilters.length - 1);
                    data.filters = strFilters;
                }

                let settings = {
                    url: config.getApiEndPoint(),
                    data: data,
                    method: "GET",
                    headers: {
                        "x-ivs-analytics": "TcXP9uF5w9EWddwP"
                    },
                    timeout: 60000,
                    success: that.ajaxDone,
                    error: that.ajaxFail
                };

                $.ajax(settings);
                that.showLoading();
            };

            this.ajaxDone = function (data) {
                eval(info['renderFunc'])(data, info);
                that.hideLoading();
            };
            this.ajaxFail = function () {
                that.hideLoading();
            };
            this.showLoading = function () {
                report_count += 1;
                $('.loading').slideDown();
            };
            this.hideLoading = function () {
                if (report_count > 0) {
                    report_count -= 1;
                }
                if (report_count == 0) {
                    $('.loading').slideUp();
                }
            };
        };

        let renderSummery = function (data, info) {
            info['htmlId'].forEach(function (id, index) {
                let val = data.data[0][index];
                let options = {
                    useEasing: true,
                    useGrouping: true,
                    separator: ','
                };
                let cUp = new CountUp(id, 0, val, 0, 0.5, options);
                if (!cUp.error) {
                    cUp.start();
                } else {
                    console.error(cUp.error);
                }
            });
        };

        let renderTable = function (data, info) {
            let header = '';
            info['headers'].forEach(function (name) {
                header += '<th>' + name + '</th>';
            });
            let title = '<h5>' + info.name + '</h5>';
            header = '<thead><tr>' + header + '</tr></thead>';

            let body;
            let tRow = '';
            let tCell;
            let filterType;
            let filterValue;
            let linkIndex;
            for (let row in data['data']) {
                tRow += '<tr>';
                data['data'][row].forEach(function (value, index) {
                    if (typeof info.skipCol !== 'undefined' && info.skipCol.includes(index)) {
                        return;
                    }
                    if (typeof info.linkedCol !== 'undefined' && typeof info.linkedCol[index] !== 'undefined') {
                        linkIndex = info.linkedCol[index];
                        filterType = data['meta'][linkIndex].name;
                        filterValue = data['data'][row][linkIndex];
                        value = '<button type="button" class="btn btn-link btn-action" data-filtertype="' + filterType +
                            '" data-filtervalue="' + filterValue + '">' + value + '</button>';
                    }
                    tCell = '<td>' + numberWithCommas(value) + '</td>';
                    if (typeof info.percentageIndexes !== 'undefined' && info.percentageIndexes.includes(index)) {
                        tCell += '<td>' + getPercentage(value, data.totals[index]) + '</td>';
                    }
                    tRow += tCell;
                });
                tRow += '</tr>';
            }
            body = '<tbody>' + tRow + '</tbody>';

            let table = title + '<table class="table table-hover table-bordered">' + header + body + '</table>';
            $('#' + info['htmlId']).html(table);
        };

        let renderAreaChart = function (data, info) {
            let lables = [];
            let values = [];
            for (let index in data['data']) {
                lables.push(data['data'][index][0]);
                values.push(data['data'][index][1]);
            }

            let canvas = document.getElementById(info['htmlId']);
            let ctx = canvas.getContext('2d');
            canvas.setAttribute("style", "display: block;");

            let chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'line',

                // The data for our dataset
                data: {
                    labels: lables,
                    datasets: [{
                        label: info["name"],
                        backgroundColor: 'rgba(82, 196, 224,0.5)',
                        borderColor: 'rgba(82, 196, 224,1)',
                        data: values,
                    }]
                },

                // Configuration options go here
                options: {}
            });
            charts.push(chart);
        };

        let getPercentage = function (val, tot) {
            let percentage = Math.round(val * 100 * 100 / tot) / 100;
            return percentage + '%';
        };

        let renderAllReports = function () {
            if (!config.getStatus()) {
                return;
            }
            let reportInfo = config.getReportsInfo();
            let filteredBy = config.getFilteredBy();
            let hideReport;
            resetAllReports(reportInfo);
            for (let key in reportInfo) {
                if (!reportInfo[key].active) {
                    continue;
                }
                if (
                    0 == filteredBy.length ||
                    typeof reportInfo[key].hiddenOnFilters == "undefined" ||
                    reportInfo[key].hiddenOnFilters.length == 0
                ) {
                    hideReport = false;
                } else {
                    hideReport = reportInfo[key].hiddenOnFilters.some(function (val) {
                        return filteredBy.includes(val);
                    });
                }
                if (!hideReport) {
                    (new Report(reportInfo[key])).render();
                }
            }
        };

        let resetAllReports = function (reportInfo) {
            for (let key in reportInfo) {
                if (!reportInfo[key].active) {
                    continue;
                }
                if ('renderSummery' == reportInfo[key].renderFunc) {
                    reportInfo[key].htmlId.forEach(function (id) {
                        $('#' + id).text('0');
                    });
                } else if ('renderAreaChart' == reportInfo[key].renderFunc) {
                    charts.forEach(function (chart) {
                        chart.destroy();
                    });
                    charts = [];
                } else {
                    $('#' + reportInfo[key].htmlId).text('');
                }
            }
        };

        (function () {
            let startDate = $("#start_date");
            let endDate = $("#end_date");
            $.datepicker.setDefaults({
                dateFormat: "yy-mm-dd"
            });
            startDate.datepicker({
                maxDate: -2
            });
            endDate.datepicker({
                maxDate: -2
            });

            startDate.on('change', function () {
                endDate.datepicker("option", "minDate", startDate.val());
            });
            endDate.on('change', function () {
                startDate.datepicker("option", "maxDate", endDate.val());
            });

            $('#btn-update').on('click', function () {
                config.setStartDate(startDate.val());
                config.setEndDate(endDate.val());
                config.setTimezone($('#timezone').val());

                renderAllReports();
            });
        })();

        let populateFilters = function () {
            let html = '';
            let filteredBy = [config.getPropertyType()];
            let filters = config.getFilters();
            for (let key in filters) {
                filteredBy.push(key);
                html += `
                <div class="filter">
                    <span class="lable">${filterLables[key]}:</span>
                    <span class="value">${filters[key].displayValue}</span>
                    <i class="fa fa-times close-filter" data-filter="${key}"></i>
                </div>
                `;
            }
            $('#filters').html(html);
            config.setFilteredBy(filteredBy);
        };

        $(document).on('click', '.btn-action', function () {
            let filterType = $(this).data('filtertype');
            let filterValue = $(this).data('filtervalue');
            let displayValue = $(this).text();
            config.setFilters(filterType, filterValue, displayValue);
            renderAllReports();
        });

        $('#filters').on('click', '.close-filter', function () {
            let filterType = $(this).data('filter');
            config.deleteFilter(filterType);
            renderAllReports();
        });

        populateFilters();
        renderAllReports();

        // win.analyticsConf = config;
    })(window, document, jQuery)
});

function numberWithCommas(number) {
    // Divide from decimal point (if there is).
    let parts = number.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}
